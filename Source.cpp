#include "Res.h"
#include <iostream>
#include <thread> // uses to execute the graphics.
#include <Windows.h> //for system functions

/* fill more #includes if needed */

#define _QUOTE(X) #X
#define QUOTE(X) _QUOTE(X)
#define RES_TYPE QUOTE(RES_TYPE_UNQUOTED)
#define RES_NAME QUOTE(RES_NAME_UNQUOTED)

//functions declerations
void createResource();
void runGraphics();

//note: you might need to increase Sleep value if the connection to the graphic file fails
//	or if after exiting the game - the graphic exe file still exists.

int main()
{
	//Black box for now - will be learned in the future.
	std::thread t(runGraphics); 
	t.detach();
	Sleep(1000);
	
	/* **complete your main** */
	
	/* end of main */
	Sleep(1000);
	remove(EXE_NAME); // removes the created graphic exe file
	return EXIT_SUCCESS;
}

//This function loads the resource and creates an exe file of it.
void createResource()
{
	HANDLE fp = nullptr;
	HRSRC res = nullptr;
	HGLOBAL global = nullptr;
	char* buf = nullptr;
	DWORD size = 0;
	DWORD written = 0;
	int ret = -1;

	//checks for graphics code loaded and created successfully.
	if (((res = FindResource(nullptr, RES_NAME, RES_TYPE)) == nullptr)
		|| (global = LoadResource(nullptr, res)) == nullptr
		|| (size = SizeofResource(nullptr, res)) == 0
		|| (buf = (char*)LockResource(global)) == nullptr
		|| (fp = CreateFile(EXE_NAME, GENERIC_WRITE, 0, nullptr, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, nullptr)) == INVALID_HANDLE_VALUE
		|| WriteFile(fp, buf, size, nullptr, nullptr) == FALSE)
	{
		std::cerr << "Error creating Graphics";
		exit(EXIT_FAILURE);
	}
	CloseHandle(fp);
}

//This funcion uses system function in order to execute the graphics.
void runGraphics()
{
	system(EXE_NAME);
}